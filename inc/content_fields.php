<?php
$editor = $form->editor( 'Content' )
                ->setLabel( __( 'Content', PLUGIN_NAME ) );

$text_alignment = $form->select( 'Text Alignment' )
                       ->setLabel( __( 'Text Alignment' ), PLUGIN_NAME )
                       ->setHelp( __( 'Determine the text alignment <a href="https://getuikit.com/docs/text#text-alignment" target="_blank">More information</a>' ) )
                       ->setOptions( array(
	                       __( 'Default', PLUGIN_NAME ) => '',
	                       __( 'Center', PLUGIN_NAME )  => 'uk-text-center',
	                       __( 'left', PLUGIN_NAME )    => 'uk-text-left',
	                       __( 'Right', PLUGIN_NAME )   => 'uk-text-right',
	                       __( 'Justify', PLUGIN_NAME ) => 'uk-text-justify',

                       ) );

$class = $form->text( 'Class' )
              ->setLabel( __( 'Class', PLUGIN_NAME ) );

$id = $form->text( 'ID' )
           ->setLabel( __( 'ID' ), PLUGIN_NAME );

$heading = $form->select( 'Heading' )
                ->setLabel( __( 'Heading Tag' ), PLUGIN_NAME )
                ->setOptions( array(
	                __( 'H1', PLUGIN_NAME ) => 'uk-h1',
	                __( 'H2', PLUGIN_NAME ) => 'uk-h2',
	                __( 'H3', PLUGIN_NAME ) => 'uk-h3',
	                __( 'H4', PLUGIN_NAME ) => 'uk-h4',
	                __( 'H5', PLUGIN_NAME ) => 'uk-h5',
	                __( 'H6', PLUGIN_NAME ) => 'uk-h6',

                ) );
$text = $form->text('Text')
->setLabel(__('Text'), PLUGIN_NAME);
