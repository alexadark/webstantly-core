<?php

add_filter( 'body_class', 'wst_add_builder_body_class' );
/**
 * Add builder-page body class
 *
 * @since 1.0.0
 *
 * @param $class
 *
 * @return array
 */
function wst_add_builder_body_class($class){

	if(is_singular('page') && tr_posts_field('builder_page')) {
		$class[] = 'builder-page';
	}
	return $class;

}

add_action( 'init', function(){
	if(tr_posts_field('builder_page')){
		remove_post_type_support( 'page', 'editor' );
	}


}, 99 );


add_action( 'genesis_entry_content', 'wst_display_page_builder',15 );
/**
 * Display page builder if page builder is checked
 *
 * @since 1.0.0
 *
 * @return void
 */
	function wst_display_page_builder(){
		if(tr_posts_field('builder_page')) {
			tr_components_field( 'modules' );
		}
	}


add_action( 'wp', function(){

	if(tr_posts_field('builder_page', 1239)){
		remove_post_type_support( 'page', 'editor' );
	}

}, 99 );







