<?php

/*-----------------------------------------------------------
	Headings on Top Image
/*------------------------------------------------------------*/

$headings = tr_meta_box('Hero text')->setLabel( __( 'Hero Text', PLUGIN_NAME ) );
$headings-> addScreen(array('page'));
$headings->setPriority('high');
$headings->setCallback(function(){
	$form = tr_form();
	echo $form->text('Hero Title')->setLabel( __( 'Hero Title', PLUGIN_NAME ) );
	echo $form->editor('Hero Subtitle')->setLabel( __( 'Hero Subtitle', PLUGIN_NAME ) );
	echo $form->text('cta text')
	->setLabel(__('Call to Action button Text'), PLUGIN_NAME)
	->setHelp( __( ' the button will appear only if you fill this box' ) );
	echo $form->text('cta url')
	          ->setLabel(__('Call to Action Url'), PLUGIN_NAME);
	echo $form->checkbox('cta target')
	          ->setLabel(__('Call to Action Target'), PLUGIN_NAME)
	          ->setHelp( __( ' if checked the link will open in a new window' ) );




});

/*-----------------------------------------------------------
	BUILDER
/*------------------------------------------------------------*/
$builder = tr_meta_box( 'Page Builder' )->setLabel( __( 'Page Builder', PLUGIN_NAME ) );
$builder->addScreen( array( 'page' ) );
$builder->setPriority( 'high' );
$builder->setCallback( function () {
	$form = tr_form();
	echo $form->builder( 'Modules' );
} );


/*-----------------------------------------------------------
	PAGES
/*------------------------------------------------------------*/
$builder = tr_meta_box( 'Builder Activated' )
	->setContext( 'side' )
	->setLabel( __( 'Builder Activated', PLUGIN_NAME ) );
$builder->addScreen( 'page' );
$builder->setCallback( function () {
	$form = tr_form();
	echo $form->checkbox( 'Builder page' )
	          ->setText( __( "Uncheck if you want don't want to use the builder", PLUGIN_NAME ) )
	          ->setLabel( __( 'Builder Page', PLUGIN_NAME ) )
	          ->setSetting( 'default', true );
} );
