<?php

$width = $form->select( 'Width' )
              ->setLabel( __( 'Container Width' ), PLUGIN_NAME )
              ->setHelp( __( 'Determine the width of the content <a href="https://getuikit.com/docs/container" target="_blank">More information</a>' ) )
              ->setOptions( array(
	              __( 'Normal', PLUGIN_NAME ) => '',
	              __( 'Xsmall', PLUGIN_NAME ) => 'uk-container-xsmall',
	              __( 'Small', PLUGIN_NAME )  => 'uk-container-small',
	              __( 'Large', PLUGIN_NAME )  => 'uk-container-large',

              ) );


$bg = $form->select( 'Background' )
           ->setLabel( __( 'Background Color' ), PLUGIN_NAME )
           ->setHelp( __( 'Determine the background color of the module, the primary, secondary and muted colors can be defined in the customizer<a href="https://getuikit.com/docs/section#style-modifiers" target="_blank">More information</a>' ) )
           ->setOptions( array(
	           __( 'Default', PLUGIN_NAME )         => 'uk-section-default',
	           __( 'Muted', PLUGIN_NAME )           => 'uk-section-muted',
	           __( 'Primary Color', PLUGIN_NAME )   => 'uk-section-primary',
	           __( 'Secondary Color', PLUGIN_NAME ) => 'uk-section-secondary',

           ) );

$margin = $form->select( 'Margin' )
               ->setLabel( __( 'Margin' ), PLUGIN_NAME )
               ->setHelp( __( 'Determine the margin between this component and the others <a href="https://getuikit.com/docs/section" target="_blank">More information</a>' ) )
               ->setOptions( array(
	               __( 'No margin', PLUGIN_NAME ) => '',
	               __( 'Normal', PLUGIN_NAME )    => 'uk-margin',
	               __( 'Small', PLUGIN_NAME )     => 'uk-margin-small',
	               __( 'Large', PLUGIN_NAME )     => 'uk-margin-large',

               ) );

$padding = $form->select( 'Padding' )
                ->setLabel( __( 'Module Padding' ), PLUGIN_NAME )
                ->setHelp( __( 'Determine the vertical padding of the module <a href="https://getuikit.com/docs/section#size-modifier" target="_blank">More information</a>' ) )
                ->setOptions( array(
	                __( 'Normal', PLUGIN_NAME )     => '',
	                __( 'No padding', PLUGIN_NAME ) => 'uk-padding-remove-vertical',
	                __( 'Xsmall', PLUGIN_NAME )     => 'uk-section-xsmall',
	                __( 'Small', PLUGIN_NAME )      => 'uk-section-small',
	                __( 'Large', PLUGIN_NAME )      => 'uk-section-large',
	                __( 'Xlarge', PLUGIN_NAME )     => 'uk-section-xlarge'
                ) );

$text_columns = $form->select( 'Text Columns' )
                     ->setLabel( __( 'Text Columns' ), PLUGIN_NAME )
                     ->setHelp( __( 'Determine the number of text columns <a href="https://getuikit.com/docs/column" target="_blank">More information</a>' ) )
                     ->setOptions( array(
	                     __( '1', PLUGIN_NAME ) => '',
	                     __( '2', PLUGIN_NAME ) => 'uk-column-1-2@m',
	                     __( '3', PLUGIN_NAME ) => 'uk-column-1-3@m',
	                     __( '4', PLUGIN_NAME ) => 'uk-column-1-4@l uk-column-1-2@m',
                     ) );

$text_divider = $form->checkbox( 'Divider' )
                     ->setLabel( __( 'Columns Divider' ), PLUGIN_NAME )
                     ->setHelp( __( 'Apply a divider between text columns, only works if there is more than 1 text column <a href="https://getuikit.com/docs/column#divider-modifier" target="_blank">More information</a>' ) );


