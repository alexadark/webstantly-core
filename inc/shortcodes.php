<?php
/**
* Page Link Shortcode
*
*/
function be_page_link_shortcode( $atts ) {
$output = '';
$atts = shortcode_atts( array(
'ids' => '',
), $atts );

$ids = array_map( 'intval', explode( ',', $atts['ids'] ) );
if( $ids ) {
$output .= '<div class="page-link">';
	foreach( $ids as $id ) {

	$style = has_post_thumbnail( $id ) ? ' style="background-image: url(' . wp_get_attachment_image_url( get_post_thumbnail_id( $id ), 'page_link' ) . ');"' : '';
	$output .= '<a href="' . get_permalink( $id ) . '"' . $style . '><span class="page-link-title">' . get_the_title( $id ) . '</span></a>';
	}
	$output .= '</div>';
}

return $output;
}
add_shortcode( 'page-link', 'be_page_link_shortcode' );
/**
* Page Link Shortcode UI
*
*/
function be_page_link_shortcode_ui() {

if( ! function_exists( 'shortcode_ui_register_for_shortcode' ) )
return;

shortcode_ui_register_for_shortcode( 'page-link', array(
'label'         => 'Page Link',
'listItemImage' => 'dashicons-admin-links',
'attrs'         => array(
array(
'label'    => 'Pages',
'attr'     => 'ids',
'type'     => 'post_select',
'query'    => array( 'post_type' => 'page'),
'multiple' => true,
)
)
) );
}
add_action( 'init', 'be_page_link_shortcode_ui' );
/**
* Page Link Image Size
*
*/
function be_page_link_image_size() {

add_image_size( 'page_link', 470, 300, true );
}
add_action( 'after_setup_theme', 'be_page_link_image_size' );