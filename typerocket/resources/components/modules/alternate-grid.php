<h1>Alternate grid</h1>
<p><?php _e('You can have several rows, then the order will be inversed on each row', PLUGIN_NAME); ?></p>
<?php

$settings = function () use ( $form ) {
echo $form->select( 'Background' )
          ->setLabel( __( 'Background Color' ), PLUGIN_NAME )
          ->setHelp( __( 'Determine the background color of the module, the primary, secondary and muted colors can be defined in the customizer<a href="https://getuikit.com/docs/section#style-modifiers" target="_blank">More information</a>' ) )
          ->setOptions( array(
	          __( 'Default', PLUGIN_NAME )         => 'uk-section-default',
	          __( 'Muted', PLUGIN_NAME )           => 'uk-section-muted',
	          __( 'Primary Color', PLUGIN_NAME )   => 'uk-section-primary',
	          __( 'Secondary Color', PLUGIN_NAME ) => 'uk-section-secondary',

          ) );

echo $form->select( 'Margin' )
          ->setLabel( __( 'Margin' ), PLUGIN_NAME )
          ->setHelp( __( 'Determine the margin between this component and the others <a href="https://getuikit.com/docs/section" target="_blank">More information</a>' ) )
          ->setOptions( array(
	          __( 'No margin', PLUGIN_NAME ) => '',
	          __( 'Normal', PLUGIN_NAME )    => 'uk-margin',
	          __( 'Small', PLUGIN_NAME )     => 'uk-margin-small',
	          __( 'Large', PLUGIN_NAME )     => 'uk-margin-large',

          ) );
};

$content = function () use ( $form ) {
echo $form->repeater( 'Photo Text' )->setLabel( __( 'Image and Text' ), PLUGIN_NAME )
    ->setFields( array(
	$form->image( 'Photo' )->setLabel( __( 'Image' ), PLUGIN_NAME ),
	$form->editor( 'Text' )->setLabel( __( 'Text' ), PLUGIN_NAME ),
) );

echo $form->text( 'Class' )
          ->setLabel( __( 'Class', PLUGIN_NAME ) );
echo $form->text( 'ID' )
          ->setLabel( __( 'ID' ), PLUGIN_NAME );
};

tr_tabs()->setForm( $form )->bindCallbacks()
         ->addTab( 'Content', $content )
         ->addTab( 'Settings', $settings )
         ->uidTabs()// Make all tab IDs unique
         ->render();
