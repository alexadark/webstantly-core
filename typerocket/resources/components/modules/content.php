<h1>Content Component</h1>
<?php
$settings = function () use ( $form ) {

	echo $form->select( 'Width' )
	          ->setLabel( __( 'Container Width' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the width of the content <a href="https://getuikit.com/docs/container" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'Normal', PLUGIN_NAME ) => '',
		          __( 'Xsmall', PLUGIN_NAME ) => 'uk-container-xsmall',
		          __( 'Small', PLUGIN_NAME )  => 'uk-container-small',
		          __( 'Large', PLUGIN_NAME )  => 'uk-container-large',

	          ) );

	echo $form->select( 'Background' )
	          ->setLabel( __( 'Background Color' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the background color of the module, the primary, secondary and muted colors can be defined in the customizer<a href="https://getuikit.com/docs/section#style-modifiers" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'Default', PLUGIN_NAME )         => 'uk-section-default',
		          __( 'Muted', PLUGIN_NAME )           => 'uk-section-muted',
		          __( 'Primary Color', PLUGIN_NAME )   => 'uk-section-primary',
		          __( 'Secondary Color', PLUGIN_NAME ) => 'uk-section-secondary',

	          ) );
	echo $form->image( 'bg Image' )
	          ->setLabel( __( 'Background Image' ), PLUGIN_NAME )
	          ->setHelp( __( 'Upload a background image' ) );

	echo $form->checkbox( 'Light' )
	          ->setLabel( __( 'Light Text' ), PLUGIN_NAME )
	          ->setHelp( __( 'Check this box if you background is dark, and you need the text to be light' ) );


	echo $form->checkbox( 'Parallax' )
	          ->setLabel( __( 'Parallax' ), PLUGIN_NAME )
	          ->setHelp( __( 'Apply a parallax effect (need to have a background image uploaded' ) );

	echo $form->select( 'Padding' )
	          ->setLabel( __( 'Module Padding' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the vertical padding of the module <a href="https://getuikit.com/docs/section#size-modifier" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'Normal', PLUGIN_NAME )     => '',
		          __( 'No padding', PLUGIN_NAME ) => 'uk-padding-remove-vertical',
		          __( 'Xsmall', PLUGIN_NAME )     => 'uk-section-xsmall',
		          __( 'Small', PLUGIN_NAME )      => 'uk-section-small',
		          __( 'Large', PLUGIN_NAME )      => 'uk-section-large',
		          __( 'Xlarge', PLUGIN_NAME )     => 'uk-section-xlarge'
	          ) );


	echo $form->select( 'Margin' )
	          ->setLabel( __( 'Margin' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the margin between this component and the others <a href="https://getuikit.com/docs/section" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'No margin', PLUGIN_NAME ) => '',
		          __( 'Normal', PLUGIN_NAME )    => 'uk-margin',
		          __( 'Small', PLUGIN_NAME )     => 'uk-margin-small',
		          __( 'Large', PLUGIN_NAME )     => 'uk-margin-large',

	          ) );


	echo $form->select( 'Text Columns' )
	          ->setLabel( __( 'Text Columns' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the number of text columns <a href="https://getuikit.com/docs/column" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( '1', PLUGIN_NAME ) => '',
		          __( '2', PLUGIN_NAME ) => 'uk-column-1-2@m',
		          __( '3', PLUGIN_NAME ) => 'uk-column-1-3@m',
		          __( '4', PLUGIN_NAME ) => 'uk-column-1-4@l uk-column-1-2@m',
	          ) );
	echo $form->checkbox( 'Divider' )
	          ->setLabel( __( 'Columns Divider' ), PLUGIN_NAME )
	          ->setHelp( __( 'Apply a divider between text columns, only works if there is more than 1 text column <a href="https://getuikit.com/docs/column#divider-modifier" target="_blank">More information</a>' ) );
};


$content = function () use ( $form ) {
	echo $form->select( 'Headline Alignment' )
	          ->setLabel( __( 'Headline Alignment' ), PLUGIN_NAME )
	          ->setOptions( array(
		          __( 'Default', PLUGIN_NAME ) => '',
		          __( 'Center', PLUGIN_NAME )  => 'uk-text-center',
		          __( 'left', PLUGIN_NAME )    => 'uk-text-left',
		          __( 'Right', PLUGIN_NAME )   => 'uk-text-right',
		          __( 'Justify', PLUGIN_NAME ) => 'uk-text-justify',

	          ) );
	echo $form->checkbox( 'Uppercase' )
	          ->setLabel( __( 'Headline Uppercase' ), PLUGIN_NAME )
	          ->setSetting( 'default', false );

	echo $form->text( 'Headline' )
	          ->setLabel( __( 'Headline' ), PLUGIN_NAME );

	echo $form->checkbox( 'Spacer' )
	          ->setLabel( __( 'Spacer' ), PLUGIN_NAME )
	          ->setHelp( __( 'Add a spacer between headline and content' ) )
	          ->setSetting( 'default', false );

	echo $form->select( 'Content Alignment' )
	          ->setLabel( __( 'Content Alignment' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the text content alignment <a href="https://getuikit.com/docs/text#text-alignment" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'Default', PLUGIN_NAME ) => '',
		          __( 'Center', PLUGIN_NAME )  => 'uk-text-center',
		          __( 'left', PLUGIN_NAME )    => 'uk-text-left',
		          __( 'Right', PLUGIN_NAME )   => 'uk-text-right',
		          __( 'Justify', PLUGIN_NAME ) => 'uk-text-justify',

	          ) );


	echo $form->editor( 'Content' )
	          ->setLabel( __( 'Content', PLUGIN_NAME ) );

	echo $form->text( 'cta text' )
	          ->setLabel( __( 'Call to Action button Text' ), PLUGIN_NAME )
	          ->setHelp( __( ' the button will appear only if you fill this box' ) );
	echo $form->text( 'cta url' )
	          ->setLabel( __( 'Call to Action Url' ), PLUGIN_NAME );
	echo $form->checkbox( 'target' )
	          ->setLabel( __( 'Call to Action Target' ), PLUGIN_NAME )
	          ->setHelp( __( ' if checked the link will open in a new window' ) );
	echo $form->select( 'flex align' )
	          ->setLabel( __( 'Button Alignment' ), PLUGIN_NAME )
	          ->setOptions( array(
		          __( 'Center', PLUGIN_NAME ) => 'uk-flex-center',
		          __( 'Left', PLUGIN_NAME )   => 'uk-flex-left',
		          __( 'Right', PLUGIN_NAME )  => 'uk-flex-right',
	          ) );
	echo $form->select( 'button type' )
	          ->setLabel( __( 'Button Type' ), PLUGIN_NAME )
	          ->setOptions( array(
		          __( 'Primary', PLUGIN_NAME )   => 'uk-button-default',
		          __( 'Default', PLUGIN_NAME )   => 'uk-button-default',
		          __( 'Secondary', PLUGIN_NAME ) => 'uk-button-secondary',
	          ) );


	echo $form->text( 'Class' )
	          ->setLabel( __( 'Class', PLUGIN_NAME ) );
	echo $form->text( 'ID' )
	          ->setLabel( __( 'ID' ), PLUGIN_NAME );
};

tr_tabs()->setForm( $form )->bindCallbacks()
         ->addTab( 'Content', $content )
         ->addTab( 'Settings', $settings )
         ->uidTabs()// Make all tab IDs unique
         ->render();


