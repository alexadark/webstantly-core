<h1>Gallery Component</h1>
<?php
$settings = function () use ( $form ) {

	echo $form->select( 'Width' )
	          ->setLabel( __( 'Container Width' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the width of the content <a href="https://getuikit.com/docs/container" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'Normal', PLUGIN_NAME )     => '',
		          __( 'Xsmall', PLUGIN_NAME )     => 'uk-container-xsmall',
		          __( 'Small', PLUGIN_NAME )      => 'uk-container-small',
		          __( 'Large', PLUGIN_NAME )      => 'uk-container-large',
		          __( 'Full Width', PLUGIN_NAME ) => 'full-width',

	          ) );

	echo $form->select( 'Background' )
	          ->setLabel( __( 'Background Color' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the background color of the module, the primary, secondary and muted colors can be defined in the customizer<a href="https://getuikit.com/docs/section#style-modifiers" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'Default', PLUGIN_NAME )         => 'uk-section-default',
		          __( 'Muted', PLUGIN_NAME )           => 'uk-section-muted',
		          __( 'Primary Color', PLUGIN_NAME )   => 'uk-section-primary',
		          __( 'Secondary Color', PLUGIN_NAME ) => 'uk-section-secondary',

	          ) );

	echo $form->select( 'Margin' )
	          ->setLabel( __( 'Margin' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the margin between this component and the others <a href="https://getuikit.com/docs/section" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'No margin', PLUGIN_NAME ) => '',
		          __( 'Normal', PLUGIN_NAME )    => 'uk-margin',
		          __( 'Small', PLUGIN_NAME )     => 'uk-margin-small',
		          __( 'Large', PLUGIN_NAME )     => 'uk-margin-large',


	          ) );
	echo $form->select( 'Padding' )
	          ->setLabel( __( 'Module Padding' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the vertical padding of the module <a href="https://getuikit.com/docs/section#size-modifier" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'Normal', PLUGIN_NAME )              => '',
		          __( 'No padding', PLUGIN_NAME )          => 'uk-padding-remove-vertical',
		          __( 'Xsmall', PLUGIN_NAME )              => 'uk-section-xsmall',
		          __( 'Small', PLUGIN_NAME )               => 'uk-section-small',
		          __( 'Large', PLUGIN_NAME )               => 'uk-section-large',
		          __( 'Xlarge', PLUGIN_NAME )              => 'uk-section-xlarge',
		          __( 'Padding bottom only', PLUGIN_NAME ) => 'uk-padding-bottom uk-padding-remove-top',
		          __( 'Padding top only', PLUGIN_NAME )    => 'uk-padding-top uk-padding-remove-bottom'
	          ) );
	echo $form
		->select( 'Number of columns' )
		->setLabel( __( 'Number of columns' ), PLUGIN_NAME )
		->setOptions( array(
			'2 columns' => 'uk-child-width-1-2@m',
			'3 Columns' => 'uk-child-width-1-3@m',
			'4 Columns' => 'uk-child-width-1-4@m',
			'5 Columns' => 'uk-child-width-1-5@m',
			'6 Columns' => 'uk-child-width-1-3@m uk-child-width-1-6@l',

		) );

	echo $form
		->select( 'Gutter' )
		->setLabel( __( 'Grid Gutter' ), PLUGIN_NAME )
		->setOptions( array(
			'Normal'    => '',
			'Small'     => 'uk-grid-small',
			'Medium'    => 'uk-grid-medium',
			'Large'     => 'uk-grid-large',
			'No gutter' => 'uk-grid-collapse',

		) );

};

$content = function () use ( $form ) {

	echo $form->repeater( 'Gallery Items' )
	          ->setLabel( __( 'Gallery Items' ), PLUGIN_NAME )
	          ->setFields( array(
		          $form->image( 'Image' )
		               ->setLabel( __( 'Image' ), PLUGIN_NAME ),
		          $form->text( 'Video' )
		               ->setLabel( __( 'Video Url' ), PLUGIN_NAME )
		               ->setHelp( __( 'Paste here the url of the video if you want to have a video in the lightbox instead of the image' ) ),
		          $form->editor( 'Overlay Content' )
		               ->setLabel( __( 'Overlay Content' ), PLUGIN_NAME )
		               ->setHelp( __( 'If you add content here, it will appear on hover with an overlay color and animation' ) ),

		          $form->select( 'Overlay' )->setLabel( __( 'Overlay' ), PLUGIN_NAME )->setOptions( array(
			          'Primary Color Overlay'   => 'uk-overlay-primary',
			          'No Overlay'              => '',
			          'Light Overlay'           => 'uk-overlay-default',
			          'Dark Overlay'            => 'uk-overlay-dark',
			          'Secondary Color Overlay' => 'uk-overlay-secondary',
			          'Gradient Overlay'        => 'uk-overlay-gradient',
		          ) ),

		          $form->select( 'Animation' )->setLabel( __( 'Overlay Animation' ), PLUGIN_NAME )->setOptions( array(
			          'Slide Top'    => 'uk-transition-slide-top',
			          'Slide Bottom' => 'uk-transition-slide-bottom',
			          'Slide Left'   => 'uk-transition-slide-left',
			          'Slide Right'  => 'uk-transition-slide-right',
			          'fade'         => 'uk-transition-fade',

		          ) )

	          ) );
	echo $form->text( 'Class' )
	          ->setLabel( __( 'Class', PLUGIN_NAME ) );
	echo $form->text( 'ID' )
	          ->setLabel( __( 'ID' ), PLUGIN_NAME );
};

tr_tabs()->setForm( $form )->bindCallbacks()
         ->addTab( 'Content', $content )
         ->addTab( 'Settings', $settings )
         ->uidTabs()// Make all tab IDs unique
         ->render();


