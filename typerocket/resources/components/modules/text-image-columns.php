<h1>Photo Text Columns</h1>
<p><?php _e('You can choose the number of columns ');?></p>
<?php
$settings = function () use ( $form ) {
	echo $form
		->select( 'Number of columns' )
		->setLabel( __( 'Number of columns' ), PLUGIN_NAME )
		->setOptions( array(
			'2 columns' => 'uk-child-width-1-2@m',
			'3 Columns' => 'uk-child-width-1-3@m',
			'4 Columns' => 'uk-child-width-1-4@m',
			'5 Columns' => 'uk-child-width-1-5@m',
			'6 Columns' => 'uk-child-width-1-3@m uk-child-width-1-6@l',

		) );

	echo $form->checkbox('Inverse')
	->setLabel(__('Inverse image text order'), PLUGIN_NAME)
	->setHelp( __( 'Have the image on top and the text below, you can use that to have an icon and a blurb below fore example ' ) )
	->setSetting('default', false);

	echo $form->select( 'Background' )
	          ->setLabel( __( 'Background Color' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the background color of the module, the primary, secondary and muted colors can be defined in the customizer<a href="https://getuikit.com/docs/section#style-modifiers" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'Default', PLUGIN_NAME )         => 'uk-section-default',
		          __( 'Muted', PLUGIN_NAME )           => 'uk-section-muted',
		          __( 'Primary Color', PLUGIN_NAME )   => 'uk-section-primary',
		          __( 'Secondary Color', PLUGIN_NAME ) => 'uk-section-secondary',

	          ) );


	echo $form->select( 'Margin' )
	          ->setLabel( __( 'Margin' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the margin between this component and the others <a href="https://getuikit.com/docs/section" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'No margin', PLUGIN_NAME ) => '',
		          __( 'Normal', PLUGIN_NAME )    => 'uk-margin',
		          __( 'Small', PLUGIN_NAME )     => 'uk-margin-small',
		          __( 'Large', PLUGIN_NAME )     => 'uk-margin-large',

	          ) );
};

$content = function () use ( $form ) {

	echo $form->repeater( 'Text Image' )->setFields( array(
		$form->editor( 'Text' ),
		$form->image( 'Image' )
	) );

	echo $form->text( 'Class' )
	          ->setLabel( __( 'Class', PLUGIN_NAME ) );
	echo $form->text( 'ID' )
	          ->setLabel( __( 'ID' ), PLUGIN_NAME );
};

tr_tabs()->setForm( $form )->bindCallbacks()
         ->addTab( 'Content', $content )
         ->addTab( 'Settings', $settings )
         ->uidTabs()// Make all tab IDs unique
         ->render();
