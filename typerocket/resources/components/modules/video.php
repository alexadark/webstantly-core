<h1>Video Component</h1>
<?php
$settings = function () use ( $form ) {

	echo $form->select( 'Width' )
	          ->setLabel( __( 'Container Width' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the width of the content <a href="https://getuikit.com/docs/container" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'Normal', PLUGIN_NAME ) => '',
		          __( 'Xsmall', PLUGIN_NAME ) => 'uk-container-xsmall',
		          __( 'Small', PLUGIN_NAME )  => 'uk-container-small',
		          __( 'Large', PLUGIN_NAME )  => 'uk-container-large',

	          ) );
	echo $form->select('Height')
	->setLabel(__('Height'), PLUGIN_NAME)
	->setHelp( __( 'Choose the height of the module ') )
		->setOptions( array(
			__( 'Small', PLUGIN_NAME ) => 'uk-height-small',
			__( 'Medium', PLUGIN_NAME ) => 'uk-height-medium',
			__( 'Large', PLUGIN_NAME )  => 'uk-height-large',
			__( 'xLarge', PLUGIN_NAME )  => 'uk-height-xlarge',

		) );

	echo $form->select( 'Padding' )
	          ->setLabel( __( 'Module Padding' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the vertical padding of the module <a href="https://getuikit.com/docs/section#size-modifier" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'Normal', PLUGIN_NAME )     => '',
		          __( 'No padding', PLUGIN_NAME ) => 'uk-padding-remove-vertical',
		          __( 'Xsmall', PLUGIN_NAME )     => 'uk-section-xsmall',
		          __( 'Small', PLUGIN_NAME )      => 'uk-section-small',
		          __( 'Large', PLUGIN_NAME )      => 'uk-section-large',
		          __( 'Xlarge', PLUGIN_NAME )     => 'uk-section-xlarge'
	          ) );

	echo $form->checkbox( 'Light' )
	          ->setLabel( __( 'Light Text' ), PLUGIN_NAME )
	          ->setHelp( __( 'Check this box if you background is dark, and you need the text to be light' ) );


	echo $form->select( 'Margin' )
	          ->setLabel( __( 'Margin' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the margin between this component and the others <a href="https://getuikit.com/docs/section" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'No margin', PLUGIN_NAME ) => '',
		          __( 'Normal', PLUGIN_NAME )    => 'uk-margin',
		          __( 'Small', PLUGIN_NAME )     => 'uk-margin-small',
		          __( 'Large', PLUGIN_NAME )     => 'uk-margin-large',

	          ) );

	echo $form->select( 'Text Columns' )
	          ->setLabel( __( 'Text Columns' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the number of text columns <a href="https://getuikit.com/docs/column" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( '1', PLUGIN_NAME ) => '',
		          __( '2', PLUGIN_NAME ) => 'uk-column-1-2@m',
		          __( '3', PLUGIN_NAME ) => 'uk-column-1-3@m',
		          __( '4', PLUGIN_NAME ) => 'uk-column-1-4@l uk-column-1-2@m',
	          ) );
	echo $form->checkbox( 'Divider' )
	          ->setLabel( __( 'Columns Divider' ), PLUGIN_NAME )
	          ->setHelp( __( 'Apply a divider between text columns, only works if there is more than 1 text column <a href="https://getuikit.com/docs/column#divider-modifier" target="_blank">More information</a>' ) );
};


$content = function () use ( $form ) {
	echo $form->text('Youtube')
	          ->setLabel(__('YouTube'), PLUGIN_NAME)
	          ->setHelp( __( 'If you want to have a YOUTUBE video as background, paste here the video ID <a href="https://cl.ly/mFLM" target="_blank">This is the ID</a>'	) );

	echo $form->text('Vimeo')
	          ->setLabel(__('Vimeo'), PLUGIN_NAME)
	          ->setHelp( __( 'If you want to have a VIMEO video as background, paste here the video ID <a href="https://cl.ly/mFtO" target="_blank">This is the ID</a>'	) );
	echo $form->file('mp4')
	          ->setLabel(__('Mp4 Video File'), PLUGIN_NAME)
	          ->setHelp( __( 'If you want to have a self hosted video you need to upload mp4 and Webm files ' ) );
	echo $form->file('webm')
	          ->setLabel(__('Webm Video File'), PLUGIN_NAME)
	          ->setHelp( __( 'If you want to have a self hosted video you need to upload mp4 and Webm files ' ) );


	echo $form->select( 'Overlay' )->setLabel( __( 'Overlay' ), PLUGIN_NAME )->setOptions( array(
		'No Overlay'              => '',
		'Primary Color Overlay'   => 'uk-overlay-primary',
		'Light Overlay'           => 'uk-overlay-default',
		'Dark Overlay'            => 'uk-overlay-dark',
		'Secondary Color Overlay' => 'uk-overlay-secondary',
		'Gradient Overlay'        => 'uk-overlay-gradient',
	) );
	echo $form->select( 'Headline Alignment' )
	          ->setLabel( __( 'Headline Alignment' ), PLUGIN_NAME )
	          ->setOptions( array(
		          __( 'Default', PLUGIN_NAME ) => '',
		          __( 'Center', PLUGIN_NAME )  => 'uk-text-center',
		          __( 'left', PLUGIN_NAME )    => 'uk-text-left',
		          __( 'Right', PLUGIN_NAME )   => 'uk-text-right',
		          __( 'Justify', PLUGIN_NAME ) => 'uk-text-justify',

	          ) );
	echo $form->checkbox( 'Uppercase' )
	          ->setLabel( __( 'Headline Uppercase' ), PLUGIN_NAME )
	          ->setSetting( 'default', false );

	echo $form->text( 'Headline' )
	          ->setLabel( __( 'Headline' ), PLUGIN_NAME );

	echo $form->checkbox( 'Spacer' )
	          ->setLabel( __( 'Spacer' ), PLUGIN_NAME )
	          ->setHelp( __( 'Add a spacer between headline and content' ) )
	          ->setSetting( 'default', false );

	echo $form->select( 'Content Alignment' )
	          ->setLabel( __( 'Content Alignment' ), PLUGIN_NAME )
	          ->setHelp( __( 'Determine the text content alignment <a href="https://getuikit.com/docs/text#text-alignment" target="_blank">More information</a>' ) )
	          ->setOptions( array(
		          __( 'Default', PLUGIN_NAME ) => '',
		          __( 'Center', PLUGIN_NAME )  => 'uk-text-center',
		          __( 'left', PLUGIN_NAME )    => 'uk-text-left',
		          __( 'Right', PLUGIN_NAME )   => 'uk-text-right',
		          __( 'Justify', PLUGIN_NAME ) => 'uk-text-justify',

	          ) );


	echo $form->editor( 'Content' )
	          ->setLabel( __( 'Content', PLUGIN_NAME ) );

	echo $form->text( 'cta text' )
	          ->setLabel( __( 'Call to Action button Text' ), PLUGIN_NAME )
	          ->setHelp( __( ' the button will appear only if you fill this box' ) );
	echo $form->text( 'cta url' )
	          ->setLabel( __( 'Call to Action Url' ), PLUGIN_NAME );
	echo $form->checkbox( 'target' )
	          ->setLabel( __( 'Call to Action Target' ), PLUGIN_NAME )
	          ->setHelp( __( ' if checked the link will open in a new window' ) );
	echo $form->select( 'flex align' )
	          ->setLabel( __( 'Button Alignment' ), PLUGIN_NAME )
	          ->setOptions( array(
		          __( 'Center', PLUGIN_NAME ) => 'uk-flex-center',
		          __( 'Left', PLUGIN_NAME )   => 'uk-flex-left',
		          __( 'Right', PLUGIN_NAME )  => 'uk-flex-right',
	          ) );
	echo $form->select( 'button type' )
	          ->setLabel( __( 'Button Type' ), PLUGIN_NAME )
	          ->setOptions( array(
		          __( 'Primary', PLUGIN_NAME )   => 'uk-button-primary',
		          __( 'Default', PLUGIN_NAME )   => 'uk-button-default',
		          __( 'Secondary', PLUGIN_NAME ) => 'uk-button-secondary',
	          ) );


	echo $form->text( 'Class' )
	          ->setLabel( __( 'Class', PLUGIN_NAME ) );
	echo $form->text( 'ID' )
	          ->setLabel( __( 'ID' ), PLUGIN_NAME );
};

tr_tabs()->setForm( $form )->bindCallbacks()
         ->addTab( 'Content', $content )
         ->addTab( 'Settings', $settings )
         ->uidTabs()// Make all tab IDs unique
         ->render();


