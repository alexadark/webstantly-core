<?php $context   = Timber::get_context();
$context['data'] = $data;
$templates       = array( 'alternate-grid.twig' );
Timber::render( $templates, $context );