<?php 

$context   = Timber::get_context();
$context['data'] = $data;
$templates = array( 'content.twig' );
Timber::render( $templates, $context );