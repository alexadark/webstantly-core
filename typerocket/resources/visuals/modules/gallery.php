<?php 
$context   = Timber::get_context();
$context['data'] = $data;
$templates = array( 'gallery.twig' );
Timber::render( $templates, $context );