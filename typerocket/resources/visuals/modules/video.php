<?php 
$context   = Timber::get_context();
$context['data'] = $data;
$templates = array( 'video.twig' );
Timber::render( $templates, $context );