<?php
/**
 * Plugin Name: Webstantly Core
 * Plugin URI: http://alexandraspalato.com/
 * Description: This contains all your site's core functionality so that it is theme independent.
 * Version: 1.0.0
 * Author: Alexandra Spalato
 * Author URI: http://alexandraspalato.com/
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License version 2, as published by the Free Software Foundation.  You may NOT assume
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

// Plugin Directory
$plugin_data = get_plugin_data( __FILE__ );
define( 'WST_DIR', dirname( __FILE__ ) );
define('PLUGIN_NAME',$plugin_data['Name']);


require( 'typerocket/init.php' );

require_once( WST_DIR . '/inc/general.php' ); // General
require_once( WST_DIR . '/inc/metaboxes.php'      );
require_once( WST_DIR . '/inc/helper-functions.php' );
require_once( WST_DIR . '/inc/shortcodes.php' );
//require_once( BE_DIR . '/inc/widget-sample.php'      ); // Sample Widget

//Options

add_filter('tr_theme_options_page', function() {
	return WST_DIR . '/inc/theme-options.php';
});

add_filter('tr_theme_options_name', function() {
	return 'wst_theme_options';
});









